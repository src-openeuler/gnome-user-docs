%global tarball_version %%(echo %{version} | tr '~' '.')

Name:           gnome-user-docs
Version:        43.0
Release:        1
Summary:        GNOME User Documentation

License:        CC-BY-3.0
URL:            http://help.gnome.org/
Source0:        https://download.gnome.org/sources/%{name}/43/%{name}-%{tarball_version}.tar.xz

BuildArch:      noarch
BuildRequires:  gettext itstool pkgconfig yelp-tools make

%description
This package contains end-user documentation for the GNOME desktop
environment.

%prep
%autosetup -p1 -n %{name}-%{tarball_version}

%build
%configure
%make_build

%install
%make_install

%find_lang %{name} --all-name --with-gnome

%files -f %{name}.lang
%license COPYING
%doc NEWS README.md

%changelog
* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.0-1
- Update to 43.0

* Thu Jun 9 2022 Chenyx <chenyixiong3@huawei.com> - 42.0-2
- License compliance rectification

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.0-1
- Upgrade to 42.0

* Mon May 31 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.5-1
- Upgrade to 3.38.5
- Update Version, Release, Source0

* Mon Nov 18 2019 liujing<liujing144@huawei.com> - 3.30.1-2
- Package init
